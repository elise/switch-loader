# Switch-Loader for Binary Ninja

## Author: **EliseZeroTwo**

This is a Work In Progress Binary Ninja Loader written in Rust for common Nintendo Switch binaries

## Currently Supports

- KIP

## Credits

- [ReSwitched for their nxo64.py loader](https://github.com/reswitched/loaders/blob/master/nxo64.py)
- [Adubbz for Ghidra Switch Loader](https://github.com/Adubbz/Ghidra-Switch-Loader)
- [SwitchBrew](https://switchbrew.org/)

## Installation Instructions

- Install Rust (MSCV is `1.56.0`)
- Clone this repository into some temporary location
- Execute `cargo build --release` inside of the directory

### Windows

Copy `./target/release/libswitch_loader.dll` into `%APPDATA%/Binary Ninja/plugins/`

### Darwin

Copy `./target/release/libswitch_loader.dylib` into `~/Library/Application Support/Binary Ninja/plugins/`

### Linux

Copy `./target/release/libswitch_loader.so` into `~/.binaryninja/plugins/`

## Minimum Version

Tested on Binary Ninja 3.0-dev

## License

This plugin is released under the [MIT license](./LICENSE)
