pub mod kip1;

use std::{borrow::Cow, collections::HashMap};

use binaryninja::{
	architecture::{ArchitectureExt, CoreArchitecture},
	binaryview::{BinaryView, BinaryViewBase, BinaryViewExt},
	section::{Section, Semantics},
	segment::Segment,
	symbol::{Symbol, SymbolType},
	types::Type,
};
use if_chain::if_chain;

use crate::{
	max,
	nxo64::{ElfSymbol, Nxo64, SegmentType},
};

fn page_pad(mut input: Vec<u8>) -> Vec<u8> {
	let input_len = input.len();
	let aligned_len = ((input_len + 0xfff) >> 12) << 12;
	input.resize(aligned_len, 0);
	input
}

fn try_unmangle<'mangled>(
	arch: &CoreArchitecture,
	mangled: &'mangled str,
) -> (Option<Type>, Cow<'mangled, str>) {
	if mangled.len() < 2 || &mangled[0..2] != "_Z" {
		return (None, mangled.into());
	}

	match binaryninja::types::Type::demangle_gnu3(arch, mangled, false) {
		Ok((lhs, rhs)) => (lhs, rhs.join("::").into()),
		Err(_) => {
			log::error!("Failed to demangle type: `{}`", mangled);
			(None, mangled.into())
		}
	}
}

fn apply_nxo64(base: u64, nxo: &Nxo64, bv: &BinaryView) {
	log::info!("Applying Nxo64");
	let file_bv = bv.parent_view().unwrap_or_else(|_| bv.to_owned());
	let arch = match CoreArchitecture::by_name("aarch64") {
		Some(x) => x,
		None => {
			log::error!("Failed to find aarch64 arch");
			return;
		}
	};

	bv.set_default_arch(&arch);

	let platform = match arch.standalone_platform() {
		Some(platform) => platform,
		None => {
			log::error!("Failed to find standalone aarch64 platform");
			return;
		}
	};

	bv.set_default_platform(&platform);

	log::info!("Adding segments");
	for segment in &nxo.segments {
		let mut segment_builder =
			Segment::new(segment.memory_offset..(segment.memory_offset + segment.size))
				.is_auto(true);

		if let Some(offset) = segment.file_offset {
			segment_builder = segment_builder.parent_backing(offset..(offset + segment.size))
		}

		segment_builder = match segment.r#type {
			SegmentType::Text => segment_builder
				.contains_code(true)
				.contains_data(true)
				.readable(true)
				.writable(false)
				.executable(true),
			SegmentType::Rodata => segment_builder
				.contains_code(false)
				.contains_data(true)
				.readable(true)
				.writable(true)
				.executable(false),
			SegmentType::Data => segment_builder
				.contains_code(false)
				.contains_data(true)
				.readable(true)
				.writable(true)
				.executable(false),
			SegmentType::Bss => segment_builder
				.contains_code(false)
				.contains_data(true)
				.readable(true)
				.writable(true)
				.executable(false),
		};
		bv.add_segment(segment_builder);
		bv.add_section(
			Section::new(
				segment.r#type.as_ref(),
				segment.memory_offset..(segment.memory_offset + segment.size),
			)
			.semantics(segment.r#type.semantics())
			.is_auto(true),
		);
	}

	log::info!("Adding sections");
	for section in &nxo.sections {
		bv.add_section(
			Section::new(
				section.name,
				section.memory_offset..(section.memory_offset + section.size),
			)
			.semantics(match &section.semantics {
				Semantics::DefaultSection => Semantics::DefaultSection,
				Semantics::ReadOnlyCode => Semantics::ReadOnlyCode,
				Semantics::ReadOnlyData => Semantics::ReadOnlyData,
				Semantics::ReadWriteData => Semantics::ReadWriteData,
				Semantics::External => Semantics::External,
			}),
		);
	}

	let undef_count = nxo.symbols.iter().fold(0, |lhs, rhs| match rhs.shndx == 0 {
		true => lhs + 1,
		false => lhs,
	});

	let last_ea = max(nxo.segments.iter().map(|x| x.memory_offset + x.size));
	let mut undef_ea = last_ea.map(|x| (((x + 0xFFF) >> 12) << 12) + 8);

	if let Some(undef_ea) = undef_ea {
		let seg_range = undef_ea..(undef_ea + (undef_count * 8));
		bv.add_segment(
			Segment::new(seg_range.clone())
				.readable(true)
				.writable(false)
				.executable(false)
				.contains_data(true)
				.contains_code(false),
		);
		bv.add_section(
			Section::new("UNDEF", seg_range)
				.section_type("XTRN")
				.semantics(Semantics::ReadOnlyData),
		);
	}

	for symbol in &nxo.symbols {
		let resolved = base + symbol.value;
		let (um_type, um_name) = try_unmangle(&arch, symbol.name.as_str());

		if symbol.shndx != 0 {
			if symbol.r#type as u64 == Nxo64::STT_FUNC {
				bv.add_auto_function(&platform, resolved);
				bv.define_auto_symbol(
					&Symbol::new(SymbolType::Function, um_name.as_ref(), resolved).create(),
				);

				if_chain! {
					if let Some(um_type) = um_type;
					if let Ok(func) = bv.function_at(&platform, resolved);
					then {
						func.set_user_type(um_type)
					}
				}
			} else {
				match um_type {
					Some(um_type) => {
						bv.define_auto_symbol_with_type(
							&Symbol::new(SymbolType::Data, um_name.as_ref(), resolved).create(),
							&platform,
							Some(&um_type),
						)
						.expect("Failed to create symbol with type");
					}
					None => {
						let sym_type = if symbol.size == 1
							|| symbol.size == 2 || symbol.size == 4
							|| symbol.size == 8
						{
							Type::int(symbol.size as usize, false)
						} else {
							Type::array(Type::int(1, false).as_ref(), symbol.size)
						};
						bv.define_auto_symbol_with_type(
							&Symbol::new(SymbolType::Data, um_name.as_ref(), resolved).create(),
							&platform,
							Some(sym_type.as_ref()),
						)
						.expect("Failed to create symbol with type");
					}
				}
			}
		} else if let Some(undef_ea) = &mut undef_ea {
			bv.define_auto_symbol_with_type(
				&Symbol::new(SymbolType::ImportAddress, um_name.as_ref(), *undef_ea).create(),
				&platform,
				Some(Type::int(8, false).as_ref()),
			)
			.expect("Failed to define imported function");
			*undef_ea += 8;
		}
	}

	let mut got_name_lookup: HashMap<u64, String> = HashMap::new();
	log::info!("Applying relocations");
	for relocation in &nxo.relocations {
		let target = base + relocation.offset;
		let sym = relocation.sym.as_ref().map(|ElfSymbol { name, .. }| try_unmangle(&arch, name));

		if let Some((sym_type, sym_name)) = &sym {
			match sym_type {
				Some(sym_type) => {
					bv.define_auto_symbol_with_type(
						&Symbol::new(SymbolType::Data, sym_name.as_ref(), target).create(),
						&platform,
						Some(Type::pointer(&arch, sym_type).as_ref()),
					)
					.expect("Failed to create symbol with type");
				}
				None => bv.define_auto_symbol(
					&Symbol::new(SymbolType::Data, sym_name.as_ref(), target).create(),
				),
			}
		}

		let (packed, offset_raw) = match relocation.r_type {
			Nxo64::R_ARM_GLOB_DAT | Nxo64::R_ARM_JUMP_SLOT | Nxo64::R_ARM_ABS32 => {
				match &relocation.sym {
					Some(sym) => {
						let offset_raw = base + sym.value;
						let packed = (offset_raw as u32).to_le_bytes().to_vec();
						(Some(packed), Some(offset_raw))
					}
					None => (None, None),
				}
			}
			Nxo64::R_ARM_RELATIVE => {
				let offset_raw =
					u32::from_le_bytes(file_bv.read_vec(relocation.offset, 4).try_into().unwrap());
				(Some(offset_raw.to_le_bytes().to_vec()), Some(offset_raw as u64))
			}
			Nxo64::R_AARCH64_GLOB_DAT | Nxo64::R_AARCH64_JUMP_SLOT | Nxo64::R_AARCH64_ABS64 => {
				match &relocation.sym {
					Some(sym) => match relocation.addend {
						Some(addend) => {
							let offset_raw = base + sym.value + addend as u64;

							if addend == 0 {
								got_name_lookup.insert(relocation.offset, sym.name.clone());
							}

							(Some(offset_raw.to_le_bytes().to_vec()), Some(offset_raw))
						}
						None => (None, None),
					},
					None => (None, None),
				}
			}
			Nxo64::R_AARCH64_RELATIVE => match relocation.addend {
				Some(addend) => {
					let offset_raw = base + addend as u64;
					(Some(offset_raw.to_le_bytes().to_vec()), Some(offset_raw))
				}
				None => (None, None),
			},
			_ => (None, None),
		};

		if_chain! {
			if let Some(text_hdr) = nxo.segments.iter().find(|&x| x.r#type == SegmentType::Text);
			if let Some(packed) = packed;
			if let Some(offset_raw) = offset_raw;
			if offset_raw != base && offset_raw != (base + 0x10) && offset_raw < (text_hdr.memory_offset + text_hdr.size);
			then {
				bv.add_auto_function(&platform, offset_raw);
				if_chain! {
					if let Some((Some(sym_type), _)) = sym;
					if let Ok(func) = bv.function_at(&platform, offset_raw);
					then {
						func.set_user_type(sym_type)
					}
				}
				bv.write(target, &packed);
			}
		}
	}

	log::info!("Defining PLT entries");
	for plt_entry in &nxo.plt_entries {
		if let Some(sym) = got_name_lookup.get(&plt_entry.target) {
			let addr = base + plt_entry.offset;
			bv.define_auto_symbol(
				&Symbol::new(SymbolType::ImportedFunction, sym.as_str(), addr).create(),
			);
		}
	}

	log::info!("Searching for BL targets");
	if let Some(seg) = nxo.segments.iter().find(|seg| seg.r#type == SegmentType::Text) {
		let text_len_div_4 = seg.size / 4;
		let text_end = seg.memory_offset + seg.size;
		for offset in 0..text_len_div_4 {
			let raw = bv.read_vec(seg.memory_offset + offset, 4);
			if raw.len() == 4 {
				let d = u32::from_le_bytes(raw.try_into().unwrap());
				if (d & 0xfc000000) == 0x94000000 {
					let mut imm = d & 0x3ffffff;

					if imm & 0x2000000 != 0 {
						imm |= !0x1ffffff
					}
					if (0..=2).contains(&imm) {
						continue;
					}

					let target = seg.memory_offset + offset + imm as u64 * 4;
					if target >= seg.memory_offset && target < text_end {
						bv.add_auto_function(&platform, target)
					}
				}
			}
		}
	}

	log::info!("Creating function at entrypoint");
	bv.add_auto_function(&platform, nxo.entrypoint);

	log::info!("Applied Nxo64");
}
