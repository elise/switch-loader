use std::{collections::HashMap, fmt::Debug};

use algos::pattern::horspool;
use binaryninja::section::Semantics;
use if_chain::if_chain;

use crate::{max, min};

struct Reader<'buf> {
	offset: usize,
	buffer: &'buf [u8],
}

impl<'buf> Reader<'buf> {
	pub fn new(buffer: &'buf [u8]) -> Self {
		Self { offset: 0, buffer }
	}

	pub fn read_u8(&mut self) -> Option<u8> {
		match self.offset + std::mem::size_of::<u8>() < self.buffer.len() {
			true => {
				let out = u8::from_le_bytes(
					self.buffer[self.offset..(self.offset + std::mem::size_of::<u8>())]
						.try_into()
						.unwrap(),
				);
				self.offset += std::mem::size_of::<u8>();
				Some(out)
			}
			false => None,
		}
	}

	pub fn read_u16(&mut self) -> Option<u16> {
		match self.offset + std::mem::size_of::<u16>() < self.buffer.len() {
			true => {
				let out = u16::from_le_bytes(
					self.buffer[self.offset..(self.offset + std::mem::size_of::<u16>())]
						.try_into()
						.unwrap(),
				);
				self.offset += std::mem::size_of::<u16>();
				Some(out)
			}
			false => None,
		}
	}

	pub fn read_u32(&mut self) -> Option<u32> {
		let end_offset = self.offset + std::mem::size_of::<u32>();
		match end_offset < self.buffer.len() {
			true => {
				let out = u32::from_le_bytes(
					self.buffer[self.offset..(self.offset + std::mem::size_of::<u32>())]
						.try_into()
						.unwrap(),
				);
				self.offset += std::mem::size_of::<u32>();
				Some(out)
			}
			false => None,
		}
	}

	pub fn read_u64(&mut self) -> Option<u64> {
		match self.offset + std::mem::size_of::<u64>() < self.buffer.len() {
			true => {
				let out = u64::from_le_bytes(
					self.buffer[self.offset..(self.offset + std::mem::size_of::<u64>())]
						.try_into()
						.unwrap(),
				);
				self.offset += std::mem::size_of::<u64>();
				Some(out)
			}
			false => None,
		}
	}

	pub fn read(&mut self, size: usize) -> Option<&'buf [u8]> {
		let end_offset = self.offset + size;
		match end_offset < self.buffer.len() {
			true => {
				let out = &self.buffer[self.offset..(self.offset + size)];
				self.offset += size;
				Some(out)
			}
			false => None,
		}
	}

	pub fn seek(&mut self, offset: u64) {
		self.offset = offset as usize;
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SegmentType {
	Text,
	Rodata,
	Data,
	Bss,
}

impl SegmentType {
	pub fn semantics(&self) -> Semantics {
		match self {
			SegmentType::Text => Semantics::ReadOnlyCode,
			SegmentType::Rodata => Semantics::ReadOnlyData,
			SegmentType::Data => Semantics::ReadWriteData,
			SegmentType::Bss => Semantics::ReadWriteData,
		}
	}
}

impl AsRef<str> for SegmentType {
	fn as_ref(&self) -> &str {
		match self {
			SegmentType::Text => ".text",
			SegmentType::Rodata => ".rodata",
			SegmentType::Data => ".data",
			SegmentType::Bss => ".bss",
		}
	}
}

#[derive(Debug, Clone, Copy)]
pub struct Segment {
	pub r#type: SegmentType,
	pub memory_offset: u64,
	pub file_offset: Option<u64>,
	pub size: u64,
}

impl Segment {
	pub fn new(
		r#type: SegmentType,
		memory_offset: u64,
		file_offset: Option<u64>,
		size: u64,
	) -> Self {
		Self { r#type, memory_offset, file_offset, size }
	}
}

pub struct Section {
	pub name: &'static str,
	pub memory_offset: u64,
	pub size: u64,
	pub semantics: Semantics,
}

impl Debug for Section {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_struct("Section")
			.field("name", &self.name)
			.field("memory_offset", &self.memory_offset)
			.field("size", &self.size)
			.finish()
	}
}

impl Section {
	pub fn new(name: &'static str, memory_offset: u64, size: u64, semantics: Semantics) -> Self {
		Self { name, memory_offset, size, semantics }
	}
}

#[derive(Debug)]
pub struct Nxo64 {
	pub aarch64: bool,
	pub segments: Vec<Segment>,
	pub sections: Vec<Section>,
	pub symbols: Vec<ElfSymbol>,
	pub relocations: Vec<ElfRelocation>,
	pub plt_entries: Vec<ElfPltEntry>,
	pub entrypoint: u64,

	pub eh_frame_hdr_size: Option<u64>,
	pub eh_frame_hdr_start: Option<u64>,
	pub module_offset: Option<u64>,
}

pub enum DtType {
	Single(u64),
	Multiple(Vec<u64>),
}

impl DtType {
	pub fn is_tag_multiple(tag: u64) -> bool {
		tag == 1
	}
}

pub struct SegmentInput {
	pub offset: u64,
	pub size: u64,
}

impl SegmentInput {
	pub fn new(offset: u64, size: u64) -> Self {
		Self { offset, size }
	}
}

#[allow(unused)]
impl Nxo64 {
	pub const DT_NULL: u64 = 0;
	pub const DT_NEEDED: u64 = 1;
	pub const DT_PLTRELSZ: u64 = 2;
	pub const DT_PLTGOT: u64 = 3;
	pub const DT_HASH: u64 = 4;
	pub const DT_STRTAB: u64 = 5;
	pub const DT_SYMTAB: u64 = 6;
	pub const DT_RELA: u64 = 7;
	pub const DT_RELASZ: u64 = 8;
	pub const DT_RELAENT: u64 = 9;
	pub const DT_STRSZ: u64 = 10;
	pub const DT_SYMENT: u64 = 11;
	pub const DT_INIT: u64 = 12;
	pub const DT_FINI: u64 = 13;
	pub const DT_SONAME: u64 = 14;
	pub const DT_RPATH: u64 = 15;
	pub const DT_SYMBOLIC: u64 = 16;
	pub const DT_REL: u64 = 17;
	pub const DT_RELSZ: u64 = 18;
	pub const DT_RELENT: u64 = 19;
	pub const DT_PLTREL: u64 = 20;
	pub const DT_DEBUG: u64 = 21;
	pub const DT_TEXTREL: u64 = 22;
	pub const DT_JMPREL: u64 = 23;
	pub const DT_BIND_NOW: u64 = 24;
	pub const DT_INIT_ARRAY: u64 = 25;
	pub const DT_FINI_ARRAY: u64 = 26;
	pub const DT_INIT_ARRAYSZ: u64 = 27;
	pub const DT_FINI_ARRAYSZ: u64 = 28;
	pub const DT_RUNPATH: u64 = 29;
	pub const DT_FLAGS: u64 = 30;
	pub const DT_GNU_HASH: u64 = 0x6ffffef5;
	pub const DT_VERSYM: u64 = 0x6ffffff0;
	pub const DT_RELACOUNT: u64 = 0x6ffffff9;
	pub const DT_RELCOUNT: u64 = 0x6ffffffa;
	pub const DT_FLAGS_1: u64 = 0x6ffffffb;
	pub const DT_VERDEF: u64 = 0x6ffffffc;
	pub const DT_VERDEFNUM: u64 = 0x6ffffffd;
	pub const STT_NOTYPE: u64 = 0;
	pub const STT_OBJECT: u64 = 1;
	pub const STT_FUNC: u64 = 2;
	pub const STT_SECTION: u64 = 3;
	pub const STB_LOCAL: u64 = 0;
	pub const STB_GLOBAL: u64 = 1;
	pub const STB_WEAK: u64 = 2;
	pub const R_ARM_ABS32: u64 = 2;
	pub const R_ARM_TLS_DESC: u64 = 13;
	pub const R_ARM_GLOB_DAT: u64 = 21;
	pub const R_ARM_JUMP_SLOT: u64 = 22;
	pub const R_ARM_RELATIVE: u64 = 23;
	pub const R_AARCH64_ABS64: u64 = 257;
	pub const R_AARCH64_GLOB_DAT: u64 = 1025;
	pub const R_AARCH64_JUMP_SLOT: u64 = 1026;
	pub const R_AARCH64_RELATIVE: u64 = 1027;
	pub const R_AARCH64_TLSDESC: u64 = 1031;

	fn read_dynstr(data: &[u8], index: usize) -> Option<&[u8]> {
		let end_index =
			data.iter().enumerate().skip(index).find_map(|(lhs, &rhs)| match rhs == b'\x00' {
				true => Some(lhs),
				false => None,
			})?;

		Some(&data[index..end_index])
	}

	pub fn parse<const HDR_SIZE: u64>(
		data: &[u8],
		base: u64,
		entrypoint: Option<u64>,
		text_md: SegmentInput,
		rodata_md: SegmentInput,
		data_md: SegmentInput,
		mut bss_md: SegmentInput,
	) -> Option<Self> {
		log::info!("data len: {}", data.len());
		let mut reader = Reader::new(data);
		let mut segments = Vec::new();
		let mut sections = Vec::new();

		reader.seek(HDR_SIZE + 4);
		let mod_offset = reader.read_u32()?;

		segments.push(Segment::new(
			SegmentType::Text,
			base + text_md.offset,
			Some(HDR_SIZE),
			text_md.size,
		));
		segments.push(Segment::new(
			SegmentType::Rodata,
			base + rodata_md.offset,
			Some(HDR_SIZE + text_md.size),
			rodata_md.size,
		));
		segments.push(Segment::new(
			SegmentType::Data,
			base + data_md.offset,
			Some(HDR_SIZE + text_md.size + rodata_md.size),
			data_md.size,
		));

		reader.seek(HDR_SIZE + mod_offset as u64);

		let magic = reader.read(4);
		log::debug!("magic: {:?}", magic);

		if magic != Some(b"MOD0") {
			return Some(Self {
				aarch64: true,
				segments,
				sections,
				symbols: Vec::new(),
				relocations: Vec::new(),
				plt_entries: Vec::new(),
				entrypoint: entrypoint.unwrap_or_default(),
				eh_frame_hdr_size: None,
				eh_frame_hdr_start: None,
				module_offset: None,
			});
		}

		let dynamic_offset = (mod_offset as i64 + reader.read_u32()? as i32 as i64) as u64;

		if bss_md.offset == 0 {
			bss_md.offset = (mod_offset as i64 + reader.read_u32()? as i32 as i64) as u64;
		}

		let dynamic_size = bss_md.offset - dynamic_offset;

		if bss_md.size == 0 {
			let bss_end = (mod_offset as i64 + reader.read_u32()? as i32 as i64) as u64;
			bss_md.size = bss_end - bss_md.offset;
		}

		reader.seek(HDR_SIZE + mod_offset as u64 + 0x10);
		let eh_frame_hdr_start = (mod_offset as i64 + reader.read_u32()? as i32 as i64) as u64;
		let eh_frame_hdr_end = (mod_offset as i64 + reader.read_u32()? as i32 as i64) as u64;
		let eh_frame_hdr_size = eh_frame_hdr_end - eh_frame_hdr_start;
		let module_offset = (mod_offset as i64 + reader.read_u32()? as i32 as i64) as u64;

		let libnx = if reader.read(4) == Some(b"LNY0") {
			let lnx_got_start = (mod_offset as i64 + reader.read_u32()? as i32 as i64) as u64;
			let lnx_got_end = (mod_offset as i64 + reader.read_u32()? as i32 as i64) as u64;
			sections.push(Section::new(
				".got",
				base + lnx_got_start,
				lnx_got_end - lnx_got_start,
				Semantics::ReadOnlyData,
			));
			true
		} else {
			false
		};

		reader.seek(HDR_SIZE + dynamic_offset);
		let tag1 = reader.read_u64()?;
		reader.seek(HDR_SIZE + dynamic_offset + 0x10);
		let tag2 = reader.read_u64()?;

		let aarch64 = !(tag1 > 0xFFFFFFFF || tag2 > 0xFFFFFFFF);
		let address_width_bytes = match aarch64 {
			true => 8,
			false => 4,
		};

		reader.seek(HDR_SIZE + dynamic_offset);

		let mut dynamic: HashMap<u64, DtType> = HashMap::new();
		dynamic.insert(Self::DT_NEEDED, DtType::Multiple(Vec::new()));

		for _ in 0..(dynamic_size >> 4) {
			let tag = match aarch64 {
				true => reader.read_u64()?,
				false => reader.read_u32()? as u64,
			};
			let val = match aarch64 {
				true => reader.read_u64()?,
				false => reader.read_u32()? as u64,
			};

			if tag == Self::DT_NULL {
				break;
			}

			if DtType::is_tag_multiple(tag) {
				match dynamic.get_mut(&tag) {
					Some(DtType::Multiple(list)) => list.push(val),
					_ => {
						log::error!("Failed to find multiple DtTag {} in map... panicking", tag);
						unreachable!()
					}
				};
			} else {
				dynamic.insert(tag, DtType::Single(val));
			}
		}

		sections.push(Section::new(
			".dynamic",
			base + dynamic_offset,
			dynamic_size,
			Semantics::ReadOnlyData,
		));

		let raw_dynstr = if_chain! {
			if let Some(&DtType::Single(strtab)) = dynamic.get(&Self::DT_STRTAB);
			if let Some(&DtType::Single(strsz)) = dynamic.get(&Self::DT_STRSZ);
			then {
				log::info!("Reading .dynstr");
				reader.seek(HDR_SIZE + strtab);
				reader.read(strsz as usize)?
			}
			else {
				&[]
			}
		};

		for (start_key, size_key, name) in &[
			(Self::DT_STRTAB, Self::DT_STRSZ, ".dynstr"),
			(Self::DT_INIT_ARRAY, Self::DT_INIT_ARRAYSZ, ".init_array"),
			(Self::DT_FINI_ARRAY, Self::DT_FINI_ARRAYSZ, ".fini_array"),
			(Self::DT_RELA, Self::DT_RELASZ, ".rela.dyn"),
			(Self::DT_REL, Self::DT_RELSZ, ".rel.dyn"),
			(Self::DT_JMPREL, Self::DT_PLTRELSZ, if aarch64 { ".rela.plt" } else { ".rel.plt" }),
		] {
			if_chain! {
				if let Some(&DtType::Single(start)) = dynamic.get(start_key);
				if let Some(&DtType::Single(size)) = dynamic.get(size_key);
				then {
					sections.push(Section::new(name, base + start, size, Semantics::ReadOnlyData))
				}
			}
		}

		let mut symbols = Vec::new();
		if_chain! {
			if let Some(&DtType::Single(symtab)) = dynamic.get(&Self::DT_SYMTAB);
			if let Some(&DtType::Single(strtab)) = dynamic.get(&Self::DT_STRTAB);
			then {
				reader.seek(HDR_SIZE + symtab);
				loop {
					if symtab < strtab && reader.offset as u64 - HDR_SIZE >= strtab {
						break;
					}

					let (st_name, st_info, st_other, st_shndx, st_value, st_size) = if aarch64 {
						(reader.read_u32()?,
						reader.read_u8()?,
						reader.read_u8()?,
						reader.read_u16()?,
						reader.read_u64()?,
						reader.read_u64()?)
					} else {
						let st_name = reader.read_u32()?;
						let st_value = reader.read_u32()?;
						let st_size = reader.read_u32()?;
						let st_info = reader.read_u8()?;
						let st_other = reader.read_u8()?;
						let st_shndx = reader.read_u16()?;
						(st_name, st_info, st_other, st_shndx, st_value as u64, st_size as u64)
					};

					if st_name as usize > raw_dynstr.len() {
						break;
					}

					log::debug!("Reading dynstr entry {}", st_name);
					let dynstr_entry = Self::read_dynstr(raw_dynstr, st_name as usize)?;
					log::debug!("Entry read.");

					let symbol = ElfSymbol::new(String::from_utf8_lossy(dynstr_entry).into_owned(), st_info, st_other, st_shndx, st_value, st_size);
					log::debug!("Symbol: {:?}", symbol);
					symbols.push(symbol);
				}
				sections.push(Section::new(".dynstr", base + symtab, (reader.offset as u64 - HDR_SIZE) - symtab, Semantics::ReadOnlyData))
			}
		};

		log::info!("Attempting to read relocations");
		let mut relocations = Vec::new();
		let mut locations = Vec::new();
		if_chain! {
			if let Some(&DtType::Single(start)) = dynamic.get(&Self::DT_REL);
			if let Some(&DtType::Single(size)) = dynamic.get(&Self::DT_RELSZ);
			then {
				for x in Self::process_relocations::<HDR_SIZE>(&mut reader, start, size, aarch64, &symbols, &mut relocations)? {
					locations.push(x)
				}
			}
		}

		if_chain! {
			if let Some(&DtType::Single(start)) = dynamic.get(&Self::DT_RELA);
			if let Some(&DtType::Single(size)) = dynamic.get(&Self::DT_RELASZ);
			then {
				for x in Self::process_relocations::<HDR_SIZE>(&mut reader, start, size, aarch64, &symbols, &mut relocations)? {
					locations.push(x)
				}
			}
		}

		let mut plt_entries = Vec::new();
		if_chain! {
			if let Some(&DtType::Single(start)) = dynamic.get(&Self::DT_JMPREL);
			if let Some(&DtType::Single(size)) = dynamic.get(&Self::DT_PLTRELSZ);
			then {
				log::info!("PLT Length: {:#X}", size);
				let plt_locations = Self::process_relocations::<HDR_SIZE>(&mut reader, start, size, aarch64, &symbols, &mut relocations)?;

				for x in &plt_locations {
					locations.push(*x);
				}

				let plt_got_start = min(plt_locations.iter().copied()).unwrap();

				let plt_got_end = max(plt_locations.iter().copied()).unwrap() + address_width_bytes;

				if let Some(&DtType::Single(plt_got_offset)) = dynamic.get(&Self::DT_PLTGOT) {
					sections.push(Section::new(".got.plt", base + plt_got_offset, size, Semantics::ReadOnlyData));
				}

				if aarch64 {
					reader.seek(HDR_SIZE);
					let text = reader.read(text_md.size as usize)?;

					let mut last = 12;
					let pattern = 0xD61F0220u32.to_le_bytes();
					loop {
						let needle = horspool(&text[last..], &pattern);

						let pos = last + match needle {
							Some(pos) => pos,
							None => break
						};

						last = pos + 1;

						if pos % 4 != 0 {
							continue;
						}

						let off = pos - 12;
						let mut buf = [0u32; 4];
						for x in 0..4 {
							buf[x] = u32::from_le_bytes(text[(off + (x * 4))..(off + 4 + (x * 4))].try_into().unwrap())
						}

						if buf[3] == 0xD61F0220 && (buf[0] & 0x9f00001f) == 0x90000010 && (buf[1] & 0xffe003ff) == 0xf9400211 {
							let base = off & !0xFFF;
							let immhi = (buf[0] as u64 >> 5) & 0x7ffff;
							let immlo = (buf[0] as u64 >> 29) & 3;
							let paddr = base as u64 + ((immlo << 12) | (immhi << 14));
							let poff = ((buf[1] as u64 >> 10) & 0xfff) << 3;
							let target = paddr + poff;
							if (plt_got_start <= target) && (target < plt_got_end) {
								plt_entries.push(ElfPltEntry::new(off as u64, target))
							}
						}
					}

					log::debug!("plt_entries: {:?}", plt_entries);
					let plt_start = min(plt_entries.iter().map(|x| x.offset)).unwrap();
					let plt_end = max(plt_entries.iter().map(|x| x.offset)).unwrap() + 0x10;
					sections.push(Section::new(".plt", base + plt_start, plt_end - plt_start, Semantics::ReadOnlyData));
				}

				if !libnx {
					let mut got_ok = false;
					let mut got_end = plt_got_end + address_width_bytes;
					while locations.contains(&got_end) && dynamic.get(&Self::DT_INIT_ARRAY).map(|x| match x {
						DtType::Single(x) => got_end < *x,
						DtType::Multiple(_) => {
							log::error!("DT_INIT_ARRAY is somehow DtType::Multiple... panicking");
							unreachable!();
						}
					}).unwrap_or(true) {
						got_ok = true;
						got_end += address_width_bytes;
					}

					if got_ok {
						sections.push(Section::new(".got", base + plt_got_end, got_end - plt_got_end, Semantics::ReadOnlyData));
					}
				}
			}
		}

		bss_md.size = ((bss_md.size + 0xFFF) >> 12) << 12;
		segments.push(Segment::new(SegmentType::Bss, base + bss_md.offset, None, bss_md.size));

		let entrypoint = match entrypoint {
			Some(x) => x,
			None => {
				let mut out = base;
				for sym in &symbols {
					if sym.name.as_str() == "_init" {
						out += sym.value;
						break;
					}
				}
				out
			}
		};

		let out = Self {
			aarch64,
			segments,
			sections,
			symbols,
			relocations,
			entrypoint,
			plt_entries,
			eh_frame_hdr_size: Some(eh_frame_hdr_size),
			eh_frame_hdr_start: Some(eh_frame_hdr_start),
			module_offset: Some(module_offset),
		};

		Some(out)
	}

	fn process_relocations<const HDR_SIZE: u64>(
		reader: &mut Reader<'_>,
		offset: u64,
		size: u64,
		aarch64: bool,
		symbols: &[ElfSymbol],
		relocations: &mut Vec<ElfRelocation>,
	) -> Option<Vec<u64>> {
		reader.seek(HDR_SIZE + offset);
		let reloc_size = match aarch64 {
			true => 0x18,
			false => 8,
		};

		let mut locations = Vec::new();
		for _ in 0..(size / reloc_size) {
			let (offset, addend, r_type, r_sym) = match aarch64 {
				true => {
					let offset = reader.read_u64()?;
					let info = reader.read_u64()?;
					let addend = Some(reader.read_u64()? as i64);
					let r_type = info & 0xFFFFFFFF;
					let r_sym = info >> 32;
					(offset, addend, r_type, r_sym)
				}
				false => {
					let offset = reader.read_u32()? as u64;
					let info = reader.read_u32()? as u64;
					let addend = None;
					let r_type = info & 0xFF;
					let r_sym = info >> 8;
					(offset, addend, r_type, r_sym)
				}
			};

			let sym = if r_sym != 0 { Some(symbols[r_sym as usize].clone()) } else { None };

			if r_type != Self::R_AARCH64_TLSDESC && r_type != Self::R_ARM_TLS_DESC {
				locations.push(offset)
			}

			relocations.push(ElfRelocation::new(offset, r_type, sym, addend))
		}

		Some(locations)
	}
}

#[derive(Debug, Clone)]
pub struct ElfSymbol {
	pub name: String,
	pub shndx: u16,
	pub value: u64,
	pub size: u64,
	pub vis: u8,
	pub r#type: u8,
	pub bind: u8,
}

impl ElfSymbol {
	pub fn new(name: String, info: u8, other: u8, shndx: u16, value: u64, size: u64) -> Self {
		Self { name, shndx, value, size, vis: other & 3, r#type: info & 0b1111, bind: info >> 4 }
	}
}

#[derive(Debug, Clone)]
pub struct ElfRelocation {
	pub offset: u64,
	pub r_type: u64,
	pub sym: Option<ElfSymbol>,
	pub addend: Option<i64>,
}

impl ElfRelocation {
	pub fn new(offset: u64, r_type: u64, sym: Option<ElfSymbol>, addend: Option<i64>) -> Self {
		Self { offset, r_type, sym, addend }
	}
}

#[derive(Debug, Clone)]
pub struct ElfPltEntry {
	pub offset: u64,
	pub target: u64,
}

impl ElfPltEntry {
	pub fn new(offset: u64, target: u64) -> Self {
		Self { offset, target }
	}
}
