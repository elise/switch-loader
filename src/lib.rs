pub mod loader;
pub mod nxo64;

use crate::loader::kip1::Kip1ViewType;

fn min<Item: Ord, Iter: Iterator<Item = Item>>(iter: Iter) -> Option<Item> {
	iter.fold(None, |lhs, rhs| match lhs {
		Some(x) if x > rhs => Some(rhs),
		Some(x) => Some(x),
		None => Some(rhs),
	})
}

fn max<Item: Ord, Iter: Iterator<Item = Item>>(iter: Iter) -> Option<Item> {
	iter.fold(None, |lhs, rhs| match lhs {
		Some(x) if x < rhs => Some(rhs),
		Some(x) => Some(x),
		None => Some(rhs),
	})
}

#[no_mangle]
pub extern "C" fn UIPluginInit() -> bool {
	binaryninja::logger::init(log::LevelFilter::Debug).expect("Failed to init logger");
	log::info!("Initialising switch-loader-rs");
	binaryninja::custombinaryview::register_view_type(
		"Kip1View",
		"Nintendo Switch KIP1 Format",
		|handle| {
			log::info!("Creating Kip1ViewType");
			Kip1ViewType { handle }
		},
	);
	true
}
