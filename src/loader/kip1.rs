use binaryninja::{
	binaryview::{BinaryView, BinaryViewBase, BinaryViewExt},
	custombinaryview::{
		BinaryViewType, BinaryViewTypeBase, CustomBinaryView, CustomBinaryViewType,
	},
};

use crate::{
	loader::{apply_nxo64, page_pad},
	nxo64::{Nxo64, SegmentInput},
};

#[derive(Debug)]
struct KipSegmentHeader {
	pub offset: u32,
	pub size: u32,
	pub binary_size: u32,
}

impl KipSegmentHeader {
	pub fn read(read_offset: u64, bv: &BinaryView) -> Option<Self> {
		let buf = bv.read_vec(read_offset, 4);
		if buf.len() != 4 {
			log::error!("Malformed Kip1SegmentHeader");
			return None;
		}
		let offset = u32::from_le_bytes(buf.try_into().unwrap());

		let buf = bv.read_vec(read_offset + 4, 4);
		if buf.len() != 4 {
			log::error!("Malformed Kip1SegmentHeader");
			return None;
		}
		let size = u32::from_le_bytes(buf.try_into().unwrap());

		let buf = bv.read_vec(read_offset + 8, 4);
		if buf.len() != 4 {
			log::error!("Malformed Kip1SegmentHeader");
			return None;
		}
		let binary_size = u32::from_le_bytes(buf.try_into().unwrap());

		Some(Self { offset, size, binary_size })
	}
}

fn blz_decompress(mut compressed: Vec<u8>) -> Result<Vec<u8>, blz_nx::Error> {
	let buffer_size = blz_nx::get_decompression_buffer_size(&compressed)?;
	let mut out_buffer = vec![0u8; buffer_size];
	blz_nx::decompress_raw(&mut compressed, &mut out_buffer)?;
	Ok(out_buffer)
}

pub struct Kip1View {
	pub handle: binaryninja::rc::Ref<binaryninja::binaryview::BinaryView>,
	pub nxo: Nxo64,
	pub is_64bit_addr: bool,
}

impl Kip1View {
	const HDR_SIZE: u64 = 0x100;
	const BASE: u64 = 0x7100000000;
}

impl AsRef<BinaryView> for Kip1View {
	fn as_ref(&self) -> &BinaryView {
		&self.handle
	}
}

impl BinaryViewBase for Kip1View {
	fn entry_point(&self) -> u64 {
		self.nxo.entrypoint
	}

	fn default_endianness(&self) -> binaryninja::Endianness {
		binaryninja::Endianness::LittleEndian
	}

	fn address_size(&self) -> usize {
		match self.is_64bit_addr {
			true => 8,
			false => 4,
		}
	}
}

unsafe impl CustomBinaryView for Kip1View {
	type Args = ();

	fn new(
		handle: &binaryninja::binaryview::BinaryView,
		_args: &Self::Args,
	) -> binaryninja::binaryview::Result<Self> {
		let file_handle = handle.parent_view().unwrap_or_else(|_| handle.to_owned());

		let raw_app_name = file_handle.read_vec(4, 0xC);
		let app_name = String::from_utf8_lossy(&raw_app_name);
		let title_id = u64::from_le_bytes(file_handle.read_vec(0x10, 8).try_into().unwrap());
		log::info!("Loading {} (TID: {:0>16X})", app_name, title_id);

		let mut flags = file_handle.read_vec(0x1f, 1)[0];

		let text_compressed = flags & 0b00001 != 0;
		let rodata_compressed = flags & 0b00010 != 0;
		let data_compressed = flags & 0b00100 != 0;
		let _is_aarch64 = flags & 0b01000 != 0;
		let is_64bit_addr = flags & 0b10000 != 0;

		let text_hdr = KipSegmentHeader::read(0x20, &file_handle).ok_or(())?;
		let rodata_hdr = KipSegmentHeader::read(0x30, &file_handle).ok_or(())?;
		let data_hdr = KipSegmentHeader::read(0x40, &file_handle).ok_or(())?;
		let bss_hdr = KipSegmentHeader::read(0x50, &file_handle).ok_or(())?;

		let mut text_raw = file_handle.read_vec(Self::HDR_SIZE, text_hdr.binary_size as usize);
		let mut rodata_raw = file_handle.read_vec(
			Self::HDR_SIZE + text_hdr.binary_size as u64,
			rodata_hdr.binary_size as usize,
		);
		let mut data_raw = file_handle.read_vec(
			Self::HDR_SIZE + text_hdr.binary_size as u64 + rodata_hdr.binary_size as u64,
			data_hdr.binary_size as usize,
		);

		if text_compressed {
			log::info!("Decompressing .text");
			text_raw = page_pad(blz_decompress(text_raw).map_err(|_| ())?);
			flags &= 0b0011_1110;
		}

		if rodata_compressed {
			log::info!("Decompressing .rodata");
			rodata_raw = page_pad(blz_decompress(rodata_raw).map_err(|_| ())?);
			flags &= 0b0011_1101;
		}

		if data_compressed {
			log::info!("Decompressing .data");
			data_raw = page_pad(blz_decompress(data_raw).map_err(|_| ())?);
			flags &= 0b0011_1011;
		}

		//handle.write(0x28, &(text_raw.len() as u32).to_le_bytes());
		//handle.write(0x38, &(rodata_raw.len() as u32).to_le_bytes());
		//handle.write(0x48, &(data_raw.len() as u32).to_le_bytes());

		file_handle.write(0x1f, &[flags]);

		file_handle.write(Self::HDR_SIZE, &text_raw);
		file_handle.write(Self::HDR_SIZE + text_raw.len() as u64, &rodata_raw);
		file_handle
			.write(Self::HDR_SIZE + text_raw.len() as u64 + rodata_raw.len() as u64, &data_raw);

		let text_len = text_raw.len();
		let rodata_len = rodata_raw.len();
		let data_len = data_raw.len();

		drop(text_raw);
		drop(rodata_raw);
		drop(data_raw);

		let total_size = Self::HDR_SIZE as usize + text_len + rodata_len + data_len;
		let raw = file_handle.read_vec(0, total_size);

		let nxo = match Nxo64::parse::<{ Self::HDR_SIZE }>(
			&raw,
			Self::BASE,
			Some(Self::BASE),
			SegmentInput::new(text_hdr.offset as u64, text_len as u64),
			SegmentInput::new(rodata_hdr.offset as u64, rodata_len as u64),
			SegmentInput::new(data_hdr.offset as u64, data_len as u64),
			SegmentInput::new(bss_hdr.offset as u64, bss_hdr.size as u64),
		) {
			Some(nxo) => nxo,
			None => {
				log::error!("Invalid Nxo64!");
				return Err(());
			}
		};

		apply_nxo64(Self::BASE, &nxo, handle);

		Ok(Self { handle: handle.to_owned(), nxo, is_64bit_addr })
	}

	fn init(&self, _args: Self::Args) -> binaryninja::binaryview::Result<()> {
		Ok(())
	}
}

pub struct Kip1ViewType {
	pub handle: BinaryViewType,
}

impl AsRef<BinaryViewType> for Kip1ViewType {
	fn as_ref(&self) -> &BinaryViewType {
		&self.handle
	}
}

impl BinaryViewTypeBase for Kip1ViewType {
	fn is_valid_for(&self, data: &BinaryView) -> bool {
		match data.read_buffer(0, 4) {
			Ok(magic) => magic.get_data() == b"KIP1",
			_ => {
				log::error!("Failed to read magic!");
				false
			}
		}
	}
}

impl CustomBinaryViewType for Kip1ViewType {
	fn create_custom_view<'builder>(
		&self,
		data: &binaryninja::binaryview::BinaryView,
		builder: binaryninja::custombinaryview::CustomViewBuilder<'builder, Self>,
	) -> binaryninja::binaryview::Result<binaryninja::custombinaryview::CustomView<'builder>> {
		builder.create::<Kip1View>(data, ())
	}
}
